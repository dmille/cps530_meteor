import { ChatLog } from '../api/chat.js';

Template.page.helpers ({
    chatlog: function() {
	return ChatLog.find({}, { sort: { createdAt: -1 }, limit: 10 } ).fetch().reverse();
    },
});

Template.message.helpers ({
    getTime: function(timedate) {
	return timedate.toLocaleTimeString()
    },
    selectClass: function(user) {

	try {
            const thisUserField = document.getElementById(username);
	}
	catch(err){
            return "chat";
	}
        const myUser = username.value;
	if( myUser == '' )
            return "chat";
	if( myUser == user) 
            return "me chat";
	else
            return "other chat";
    },
});

Template.body.events ({
    'submit .new-msg'(event) {
    //prevent default browser form submit
    event.preventDefault();

    //get val
    const target = event.target;
    const text = target.text.value;
    const user = document.getElementById("username").value;
    
    //insert a task into the collection
    ChatLog.insert({
        text,
        user,
        createdAt: new Date(),
    });

    // Clear form after
    target.text.value = '';
    },

    'submit .user-name' (event) {
    event.preventDefault();
    },
    
});
