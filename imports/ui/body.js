import { Template } from 'meteor/templating';
import { ChatLog } from '../api/tasks.js';

import './body.html';

Template.body.helpers({
    chatlog() {
	return ChatLog.find({}, {Sort: {DateTime:-1, limit: 10}});
    },
});
	
Template.body.events({
    'submit .new-tasks'(event) {
	//prevent default browser form submit
	event.preventDefault();

	//get val
	const target = event.target;
	const text = target.text.value;

	//insert a task into the collection
	ChatLog.insert({
	    text,
	    createdAt: new Date(),
	});

	// Clear form after
	target.text.value = '';
    },
});
